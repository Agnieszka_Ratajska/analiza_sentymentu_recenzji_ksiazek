# -*- coding: utf-8 -*-
"""4_pre_processing.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1QsYu200Z7K5MpcZp_vEEpFt5E-TXK3od

Wstępne przetwarzanie danych
"""

import pandas as pd
import numpy as np

df = pd.read_csv("reviews_all.csv")

df1 = pd.read_excel("review_links_books_1a.xlsx")

df1.info()

df.info()

df.columns

"""# W pliku wyjściowym z pobranymi linkami do recenzji mam 5972 rekordy.
# W pliku "ostatecznym" mam 2 rekordy więcej.
# Prawdopodobnie podczas pobierania recenzji, któreś linki się zdublowały.
# Usunę duble (może jest ich więcej)
"""

# Usywam duplikaty
df = df.drop_duplicates(["Link", "Autor", "Tytuł", "Data_recenzji", "Treść_recenzji", "Średnia_ocen", "Liczba_ocen", "Liczba_czytelników"])

df.info()

"""# Na chwilę obecną mam 5972 rekordy.
# Teraz typ zmiennych 'Liczba_ocen', 'Liczba_czytelników' zamienię na int.
# Typ zmiennej 'Data_recenzji' zmienię na date.
"""

# Zmieniam typ zmiennej "Liczba_ocen"
df["Liczba_ocen"] = df["Liczba_ocen"].fillna(0).astype(int)

# Zmieniam typ zmiennej "Liczba_czytelników"
df["Liczba_czytelników"] = df["Liczba_czytelników"].fillna(0).astype(int)

# Zmieniam typ zmiennej "Data_recenzji"
df["Data_recenzji"] = pd.to_datetime(df["Data_recenzji"], format="%Y-%m-%d")

df.info()

# Resetuję indeksy
df = df.reset_index(drop=True)

# Usuwam pierwsza kolumnę "Unnamed: 0"
df = df.drop(df.columns[0], axis=1)

df.info()

"""# W kilku przypadkach mam braki danych"""

# Wyświetlam braki danych we wszystkich zmiennych
df[df.isna().any(axis=1)]

"""**NaN (Not a Number); NaT (Not a Time)

# W 4 przypadkach nie mam pobranych żadnych recenzji. Sprawdziłam, i wiem, że te recenzje zostały usunięte. Więc te 4 rekordy usunę (index 4700, 4751, 4766, 4767)
"""

# Usuwam rekordy dla indeksów: 4700, 4751, 4766, 4767
to_drop = [4700, 4751, 4766, 4767]
df = df.drop(to_drop, axis=0)

"""# Teraz uzupełnię braki w datach recenzji"""

# Uzupełniam braki danych w datach na podstawie najbliższych rekordów.
# Użyję metody "ffill" - uzupełnia braki danych wartościami z poprzednich rekordów (metoda "bfill" z następnych rekordów).
df["Data_recenzji"] = df["Data_recenzji"].fillna(method="ffill")

df.iloc[5415]

df.iloc[5416]

df.info()

# Zmieniam typ zmiennej "Data_recenzji" podczas powyższych zmian wskoczył mi dodatkowo format godziny, nie potrzebuję tego.
df["Data_recenzji"] = pd.to_datetime(df["Data_recenzji"], format="%Y-%m-%d")

# Sprawdzę czy dana książka ma tylko jedną recenzję czy więcej i zapisze te rekordy do osobnej df
# keep=False oznacza, że wszystkie wystąpienia duplikatów zostaną oznaczone jako True
df_duplicate_books = df[df.duplicated(subset=["Autor", "Tytuł"], keep=False)]

# Nie ma tych rekordów dużo, więc chcę je wszystkie wyświetlić.
pd.set_option("display.max_rows", None)

df_duplicate_books.sort_values(by=["Autor", "Tytuł", "Data_recenzji"])

## ** opcjonalnie** Zapisuję do ramki danych i pliku xlsx te duplikaty
duplicate_books = pd.DataFrame(df_duplicate_books)
duplicate_books.to_excel("duplicate_books.xlsx")

"""### Jak widać, kilka tytułów (książek) ma więcej niż jedną tzw. oficjalna recenzję.
### Do tego w przypadku 6 książek, recenzja jest ta sama (ten sam autor i ta sama treść). Poza linkiem różnią się datą dodania.
### Muszę zwrócić uwagę, co do treści recenzji - sama treść jest identyczna, natomiast podczas scrapowania zaciągnął się fragment dotyczący ile osób poleca recenzje pod danym linkiem. I tu się rozjeżdża ta identyczność. Dlatego podczas usuwania rekordów nie będę odwoływała się do zmiennej 'Treść_recenzji'.
### Na potrzeby tego projektu, zostawię recenzje wcześniejsze (dotyczy to 6 rekordów).
### Pozostałe tytuły będą miały wszystkie zeskarpowane recenzje (mają róznych autorów, więc uważam, że warto to zostawić).
"""

# W związku z tym, że podczas scrapowania nie pobrałam autorów recenzji
#(żeby nie marnować czasu na ponowne scrapowanie - sprawdziłam te 6 tytułów w necie),
# do usunięcia zbędnych rekordów posłużę się zmiennymi "Autor", "Tytuł", "Data_recenzji" oraz indeksami.
rows_to_drop = df.query("index in [1116, 1173, 2892, 1379, 87, 3069]")

# Usuwam rekordy dla indeksów: 1116, 1173, 2892, 1379, 87, 3069
to_drop = [1116, 1173, 2892, 1379, 87, 3069]
df = df.drop(to_drop, axis=0)

df.info()

"""# Dodam jeszcze zmienną z samym rokiem. Później może się przydać."""

df["Rok_recenzji"] = df["Data_recenzji"].dt.year

# To będzie moja wyjściowa ramka danych do dalszych prac. Zapiszę ją do pliku csv.
reviews_eda1 = pd.DataFrame(df)
reviews_eda1.to_csv("reviews_eda1.csv")